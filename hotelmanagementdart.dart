import 'package:hotelmanagementdart/hotelmanagementdart.dart' as hotelmanagementdart;
import 'package:collection/collection.dart';
import 'dart:io';

const outputFileName = "output.txt";
const inputFileName = "input.txt";

const String commandCreateHotel = 'create_hotel';
const String commandBook = 'book';
const String commandListAvailableRoom = 'list_available_rooms';
const String commandCheckOut = 'checkout';
const String commandListGuest = 'list_guest';
const String commandListGuestByAge = 'list_guest_by_age';
const String commandListGuestByFloor = 'list_guest_by_floor';
const String commandGetGuestInRoom = 'get_guest_in_room';
const String commandCheckOutByFloor = 'checkout_guest_by_floor';
const String commandBookByFloor = 'book_by_floor';

class Command {
  String name;
  List<dynamic> params;

  Command({
    required this.name,
    required this.params,
  });
}

class Hotel {
  //Can only be 1-9
  final int floor;
  //Can only be 1-99
  final int roomPerFloor;
  final List<Room> rooms;
  //Amount of keyCard = Amount of room
  //Assign keyCard by least available number(index)
  final List<bool> availableKeyCards;

  static const limitFloor = 9;
  static const limitRoomPerFloor = 99;

  factory Hotel({required int floor,required int roomPerFloor}){
    //Make sure room and floor won't exceed limit nor below 1
    if (floor > limitFloor) {
      floor = limitFloor;
    } else if (floor == 0) {
      floor = 1;
    }
    if (roomPerFloor > limitRoomPerFloor) {
      roomPerFloor = limitRoomPerFloor;
    } else if (roomPerFloor == 0) {
      roomPerFloor = 1;
    }

    int totalRoom = floor * roomPerFloor;

    //Generate rooms for hotel
    List<Room> rooms = generateRoom(floor, roomPerFloor);

    //Generate keyCards for hotel. Since there is no booking yet, every keyCard is available
    List<bool> availableKeyCards = List.generate(totalRoom, (index) => true);

    return Hotel.create(floor: floor, roomPerFloor: roomPerFloor, rooms: rooms, availableKeyCards: availableKeyCards);
  }

  Hotel.create({
    required this.floor,
    required this.roomPerFloor,
    required this.rooms,
    required this.availableKeyCards
  });

  static List<Room> generateRoom(int floor,int roomPerFloor){
    int totalRoom = floor * roomPerFloor;
    int floorNumber = 0;
    List<Room> rooms = List.generate(totalRoom, (index) {
      //Handle floor change by index
      if (index % roomPerFloor == 0) {
        floorNumber+=1;
      }
      //Get roomNumber by index, Also make sure that it has 2 digit
      String roomNumber = ((index % roomPerFloor)+1).toString().padLeft(2,'0');
      return Room(id: int.parse("$floorNumber$roomNumber"));
    });
    return rooms;
  }
}

class Room {
  int id;
  Guest? guest;

  Room({
    required this.id,
    this.guest,
  });
}

class Guest {
  String name;
  int age;
  int roomId;
  int keyCardNumber;

  Guest({
    required this.name,
    required this.age,
    required this.roomId,
    required this.keyCardNumber,
  });
}

late String filePath;
late Hotel hotel;
bool isWriteFirstLine = true;

void main() {
  filePath = "${Directory.current.path}/lib/";
  //Clear old file before run new commands
  File outputFile = File("$filePath$outputFileName");
  if(outputFile.existsSync()){
    outputFile.delete();
  }
  isWriteFirstLine = true;

  List<Command> commands = getCommandsFromFileName("$filePath$inputFileName");
  executeCommand(commands);
}

List<Command> getCommandsFromFileName(String fileName) {
  var file = File(fileName).readAsStringSync();
  return file
      .split("\n")
      .map((line) => line.split(" "))
      .map((params) {
    //Params[0] is command, the rest are params
    String name = params[0];
    params.removeAt(0);
    return Command(name: name, params: params.map((param) {
      var parsedParam = int.tryParse(param);
      return parsedParam ?? param;
    }).toList());
  }).toList();
}

//Assume that the command and params is always valid,
//so I won't do validation on command in this project.
void executeCommand(List<Command> commands) {
  commands.forEach((command) {
    switch (command.name) {
      case commandCreateHotel:
        {
          hotel = Hotel(
              floor: command.params[0],
              roomPerFloor: command.params[1]);
          logProcess("Hotel created with ${hotel.floor} floor(s), ${hotel.roomPerFloor} room(s) per floor.");
          return;
        }
      case commandBook :{
        int roomId = command.params[0];
        String guestName = command.params[1];
        int guestAge = command.params[2];

        if(hotel.rooms.any((room) => room.id==roomId)){
          bookRoom(roomId, guestName, guestAge);
        }else{
          logProcess("Room does not exist");
        }
        return;
      }
      case commandListAvailableRoom :{
        listAvailableRoom();
        return;
      }
      case commandCheckOut :{
        int keyCardNumber = command.params[0];
        String guestName = command.params[1];

        checkOut(keyCardNumber,guestName);
        return;
      }
      case commandListGuest :{
        listGuest();
        return;
      }
      case commandListGuestByAge :{
        String comparator = command.params[0];
        int age = command.params[1];

        listGuestByAge(comparator,age);
        return;
      }
      case commandListGuestByFloor :{
        int floor = command.params[0];

        listGuestByFloor(floor);
        return;
      }
      case commandGetGuestInRoom :{
        int roomId = command.params[0];

        getGuestInRoom(roomId);
        return;
      }
      case commandCheckOutByFloor :{
        int floor = command.params[0];

        checkOutGuestByFloor(floor);
        return;
      }
      case commandBookByFloor :{
        int floor = command.params[0];
        String guestName = command.params[1];
        int guestAge = command.params[2];

        bookByFloor(floor,guestName, guestAge);
        return;
      }
      default:
        return;
    }
  });
}

void bookRoom(int roomId,String guestName,int guestAge){
  Room? bookingRoom = getRoomById(roomId);

  if(bookingRoom == null){
    logProcess("This room does not exist");
    return;
  }

  if(bookingRoom.guest!=null){
    //Room is occupied
    logProcess("Cannot book room $roomId for $guestName, The room is currently booked by ${bookingRoom.guest!.name}.");
  }else{
    //Room is available
    int keyCardNumber = getAvailableKeyCard();
    if(keyCardNumber == 0){
      //Case no keyCard is available
      logProcess("No key card available");
      return;
    }else{
      //Book the room
      bookingRoom.guest = Guest(name: guestName, age: guestAge, roomId: roomId, keyCardNumber: keyCardNumber);
      //Assign the key card, so it is now unavailable
      //indexOfKeyCard = keyCardNumber - 1
      hotel.availableKeyCards[keyCardNumber-1] = false;
    }
    logProcess("Room $roomId is booked by $guestName with keycard number $keyCardNumber.");
  }
}

int getAvailableKeyCard() {
  //Get least number available key card (since it is index)
  //keyCardNumber = indexOfKeyCard + 1
  int keyCardNumber = hotel.availableKeyCards.indexWhere((availableKeyCard) => availableKeyCard == true) + 1;
  return keyCardNumber;
}

Room? getRoomById(int roomId) {
  return hotel.rooms.firstWhereOrNull((room) => room.id == roomId);
}

void listAvailableRoom() {
  List<Room> availableRooms = hotel.rooms.where((room) => room.guest == null).toList();
  if (availableRooms.isEmpty) {
    logProcess("No room available");
  } else {
    String availableRoomString = availableRooms.map((room) => room.id).join(",");
    logProcess(availableRoomString);
  }
}

void checkOut(int keyCardNumber,String guestName){
  Room? roomOfThisCard = hotel.rooms.firstWhereOrNull((room) => room.guest?.keyCardNumber == keyCardNumber);
  Guest? keyCardOwner = roomOfThisCard?.guest;

  if(keyCardOwner == null){
    logProcess("Cannot find the room with this keycard");
    return;
  }

  if(keyCardOwner.name != guestName){
    //This keyCard is not own by this guest
    logProcess("Only ${keyCardOwner.name} can checkout with keycard number $keyCardNumber.");
  }else{
    //This keyCard is own by this guest
    logProcess("Room ${keyCardOwner.roomId} is checkout.");
    //Remove guest from this room
    roomOfThisCard!.guest = null;
    //After check out,the keyCard is available
    hotel.availableKeyCards[keyCardNumber-1] = true;
  }
}

void listGuest() {
  List<Guest> currentGuests = hotel.rooms
      .where((room) => room.guest != null)
      .map((room) => room.guest!)
      .toList();
  //Sort guest by keyCardNumber before display
  currentGuests.sort((a,b)=>a.keyCardNumber.compareTo(b.keyCardNumber));

  if (currentGuests.isEmpty) {
    logProcess("No guest currently stay in this hotel");
  } else {
    //Not sure if should remove duplicate name
    //String guestNames = currentGuests.map((guest) => guest.name).toSet().join(",");
    String guestNames = currentGuests.map((guest) => guest.name).join(", ");
    logProcess(guestNames);
  }
}

void listGuestByAge(String comparator, int age) {
  List<Guest> currentGuestsByAge = hotel.rooms
      .where((room) {
        if (room.guest != null) {
          if (comparator == "<") {
            return room.guest!.age < age;
          } else if (comparator == "<=") {
            return room.guest!.age <= age;
          } else if (comparator == ">") {
            return room.guest!.age > age;
          } else if (comparator == ">=") {
            return room.guest!.age >= age;
          } else if (comparator == "=") {
            return room.guest!.age == age;
          } else if (comparator == "==") {
            return room.guest!.age == age;
          } else {
            return false;
          }
        } else {
          return false;
        }
      })
      .map((room) => room.guest!)
      .toList();

  if (currentGuestsByAge.isEmpty) {
    logProcess("No guest by this age");
  } else {
    //Not sure if should remove duplicate name
    //String guestNames = currentGuestsByAge.map((guest) => guest.name).toSet().join(",");
    String guestNames = currentGuestsByAge.map((guest) => guest.name).join(", ");
    logProcess(guestNames);
  }
}

void listGuestByFloor(int floor){
  List<Guest> guestInThisFloor = hotel.rooms
      .whereIndexed((index, room) =>
          index >= (floor - 1) * hotel.roomPerFloor &&
          index < floor * hotel.roomPerFloor &&
          room.guest != null)
      .map((room) => room.guest!)
      .toList();

  if (guestInThisFloor.isEmpty) {
    logProcess("No guest currently stay in this floor");
  } else {
    //Not sure if should remove duplicate name
    //String guestNames = currentGuests.map((guest) => guest.name).toSet().join(",");
    String guestNames = guestInThisFloor.map((guest) => guest.name).join(", ");
    logProcess(guestNames);
  }
}

void getGuestInRoom(int roomId){
  Room? room =  hotel.rooms.firstWhereOrNull((room) => room.id == roomId);
  if(room == null){
    logProcess("This room does not exist");
    return;
  }

  if(room.guest == null){
    logProcess("No guest stay in this room");
  }else{
    logProcess(room.guest!.name);
  }
}

void checkOutGuestByFloor(int floor) {
  List<Room> checkOutRooms = [];
  //Room by default is already sort by index, so we can query the room in each floor by index.
  hotel.rooms.forEachIndexed((index, room) {
    //Room is in this floor, so check out
    if (index >= (floor - 1) * hotel.roomPerFloor &&
        index < floor * hotel.roomPerFloor) {
      //Ignore room without guest (no need to check out)
      if (room.guest != null) {
        int keyCardNumber = room.guest!.keyCardNumber;
        //After check out,the keyCard is available
        hotel.availableKeyCards[keyCardNumber - 1] = true;
        //Remove guest from this room
        room.guest = null;

        checkOutRooms.add(room);
      }
    }
  });
  String checkOutRoomString = checkOutRooms.map((room) => room.id).join(", ");
  logProcess("Room $checkOutRoomString are checkout.");
}

void bookByFloor(int floor, String guestName, int guestAge) {
  List<Room> roomsInThisFloor = hotel.rooms
      .whereIndexed((index, element) =>
          index >= (floor - 1) * hotel.roomPerFloor &&
          index < floor * hotel.roomPerFloor)
      .toList();

  //If there is any room occupied, booking cannot be proceeded
  if (roomsInThisFloor.any((room) => room.guest != null)) {
    logProcess("Cannot book floor $floor for ${guestName}.");
    return;
  }

  List<String> assignKeyCardNumbers = [];
  //All room should be available so just book the room.
  roomsInThisFloor.forEachIndexed((index, room) {
    int keyCardNumber = getAvailableKeyCard();
    if (keyCardNumber == 0) {
      //Case no keyCard is available
      logProcess("No key card available");
    } else {
      //Book the room
      room.guest = Guest(name: guestName, age: guestAge, roomId: room.id, keyCardNumber: keyCardNumber);
      //Assign the key card, so it is now unavailable
      //indexOfKeyCard = keyCardNumber - 1
      hotel.availableKeyCards[keyCardNumber - 1] = false;

      assignKeyCardNumbers.add(keyCardNumber.toString());
    }
  });

  String bookRoomsString = roomsInThisFloor.map((room) => room.id).join(", ");
  String bookKeyCardString = assignKeyCardNumbers.join(", ");
  logProcess("Room $bookRoomsString are booked with keycard number $bookKeyCardString");
}

void logProcess(String log) {
  print(log);
  if(isWriteFirstLine){
    isWriteFirstLine = false;
  }else{
    log = "\n$log";
  }
  File("$filePath$outputFileName").writeAsStringSync(log,mode: FileMode.append);
}